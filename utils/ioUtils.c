//
// Created by fabien on 23/01/2021.
//

#include "socketClient.h"
#include "ioUtils.h"
#include <string.h>
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

char* io_userName = NULL;
char* io_password = NULL;

/*
 * ioClient.c
 *
 *  Created on: 4 janv. 2021
 *      Author: Fabien
 */

/**************************************************
 * Encodage et décodage des URI
 **************************************************/

/**
 * Demande une saisie à l'utilisateur
 * La chaine retournée est allouée dynamiquement. Donc a free.
 * @author Dorian Gardes
 * @date 30/01/2021
 * @param message Message à afficher pour demander la saisie.
 * @return La chaine saisie par l'utilisateur
 */
char* demanderSaisie(char* message) {
	if (message == NULL) message = "Saisie demandée";
	printf("%s%s%s\n", IPRE, message, GREEN);
	char* saisie = NULL;
	size_t len = 0;
	ssize_t nbrLu = 0;
	nbrLu = getline(&saisie, &len, stdin);
	printf("%s\n", RESET);
	while (nbrLu <= 0) {
		printf("%sErreur de lecture veuillez réessayer.", IPRE);
		while (getchar() != '\n');
		nbrLu = getline(&saisie, &len, stdin);
	}
	saisie[strlen(saisie) - 1] = '\0';
	return saisie;
}

/**
 * Demande une saisie d'un nombre à l'utilisateur
 * @author Dorian Gardes
 * @date 30/01/2021
 * @param message Message à afficher pour demander la saisie.
 * @param borneMin Valeur minimale (incluse) attendue
 * @param borneMax Valeur maximale (incluse) attendue
 * @return Le nombre saisi
 */
int demanderSaisieNombre(char* message, int borneMin, int borneMax) {


	bool fini = false;
	int retour = borneMin -1;
	char* end = NULL;
	while (!fini) {
		// On demande une saisie
		char* saisie = demanderSaisie(message);
		retour = (int)strtol(saisie, &end, 10);
		// Si toute la chaine n'a pas été convertie
		if (*end != '\0') {
			printf("%sVeuillez entrer un nombre !\n", DPRE);
		} else if (retour >= borneMin && retour <= borneMax) {
			fini = true;
		} else {
			printf("%sLa valeur doit être comprise entre %d et %d\n", DPRE, borneMin, borneMax);
		}
		free(saisie);
	}
	return retour;

}

/**
 * Vide l'écran
 * Ne fonctionne pas dans la console intégrée de CLion mais nickel sur un véritable terminal
 */
void clearScreen() {
	printf("\e[1;1H\e[2J"); // Volé sur https://www.geeksforgeeks.org/clear-console-c-language/
}

void stripString(char* str){
    str[strlen(str) - 1] = 0;
}

/**
 * Encode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_encoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string décodée
 * @return string encodée
 */
char* encodeURI(const char *from) {

	if (from == NULL) return NULL;
    // On convertit la chaine en chaine de wide char
    wchar_t* wideFrom = calloc(strlen(from) +3, sizeof(wchar_t));
    mbstowcs(wideFrom, from, strlen(from));
    wchar_t* fromBackup = wideFrom; // On le conserve pour le nettoyage

    wchar_t* wideTo = calloc((wcslen(wideFrom) * 3) + 1, sizeof(wchar_t));
    wchar_t* toBackup = wideTo; // On backup puisque to est utilisé dans le while
    for (; *wideFrom; wideFrom++) {
        if (rfc3986[*wideFrom]) swprintf(wideTo, sizeof(wchar_t), L"%lc", rfc3986[*wideFrom]);
        else swprintf(wideTo, 3 * sizeof(wchar_t), L"%%%02X", *wideFrom);
        while (*++wideTo);
    }

    // On alloue la taille max que prendra la chaine
    char* to = calloc((wcslen(toBackup) +2) * 4, sizeof(char));
    // On convertit les wide char en char
    wcstombs(to, toBackup, (wcslen(toBackup) +2) * 4);
    free(fromBackup);
    free(toBackup);

    // On redimensionne à la taille réelle
    to = realloc(to, (strlen(to) +2) * sizeof(char));
    return to;
}

int ishex(int x) {
    return	(x >= '0' && x <= '9')	|| (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F');
}

/**
 * Décode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_decoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string encodée
 * @return string decodée
 */
char* decodeURI(const char *from) {

	if (from == NULL) return NULL;
    // On convertit la chaine en chaine de wide char
    wchar_t* wideFrom = calloc(strlen(from) + 2, sizeof(wchar_t));
    mbstowcs(wideFrom, from, strlen(from));
    wchar_t* fromBackup = wideFrom;

    wchar_t* wideTo = calloc(wcslen(wideFrom) +2, sizeof(wchar_t));
    wchar_t *o;
    const wchar_t *end = wideFrom + wcslen(wideFrom);
    int c;

    for (o = wideTo; wideFrom < end; o++) {
        c = *wideFrom++;
        if (c == L'%' && (	!ishex(*wideFrom++)	|| !ishex(*wideFrom++) || !swscanf(wideFrom - 2, L"%2x", &c)))
            break; // On force la sortie de la boucle
        if (wideTo) *o = c;
    }

    // On alloue la taille max que prendra la chaine
    char* to = calloc((wcslen(wideTo) +2) * 4, sizeof(char));
    // On convertit les wide char en char
    wcstombs(to, wideTo, (wcslen(wideTo) +2) * 4);
    free(fromBackup);
    free(wideTo);

    // On redimensionne à la taille réelle
    to = realloc(to, (strlen(to) +2) * sizeof(char));
    return to;
}

/**
 *
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param req Chaine contenant la requête
 * @param tableau Pointeur vers le tableau défini et rempli par la focntion
 * @return Le nombre d'éléments dans tableau
 */
int decoupageRequete(const char* req, char*** tableau) {

    // On se fait une copie modifiable
    char* requete = calloc(strlen(req) +2, sizeof(char));
    strcpy(requete, req);

    // On commence par retirer le '\n' de fin
    requete[strlen(requete) -1] = '\0';

    int compteCases = 0;
    char* lastPosition = requete;
    // On sort par le break donc non ce n'est pas une boucle infinie
    while (1) {

        char* position = strchr(lastPosition, ' ');
        long len;
        if (position) len = position - lastPosition ;
        else len = (long) strlen(lastPosition);

        // On ajoute une case au tableau
        compteCases++;
#if defined(WIN32) || defined(__CYGWIN__)
        *tableau = realloc(*tableau, (compteCases +1) * sizeof(char*));
#else
        *tableau = reallocarray(*tableau, compteCases +1, sizeof(char*));
#endif

        char* temp = calloc(len +2, sizeof(char));
        strncpy(temp, lastPosition, len);
        (*tableau)[compteCases-1] = decodeURI(temp);
        free(temp);

        // Le if permet de sortir proprement si la trame est trop courte ou sans arguments
        if (position) lastPosition = position + 1; // +1 pour sauter l'espace
        else break;
    }
    lastPosition = NULL;
    free(requete);

    return compteCases;
}

void setLogin(char *userName, char *password) {

	if (userName != NULL) {
		if (io_userName != NULL) free(io_userName);
		io_userName = encodeURI(userName);
	}
	if (io_password != NULL) free(io_password);
    io_password = encodeURI(password);
}

/**
 * Affichage de la réponse du serveur
 * @author Dorian Gardes
 * @return Code de retour envoyé par le serveur
 */
int printReponse() {

	int codeRetour = -1;
	char* reponse = Reception();

	if (reponse == NULL) printf("%s%sErreur de réception de la réponse du serveur.%s\n", DPRE, RED, RESET);
	else {

		if (strncmp("AUPSCG ", reponse, 7) != 0) printf("%s%sProtocole non reconnu%s\n", DPRE, RED, RESET);
		else {
			char* decReponse = decodeURI(reponse + 7);

			if (strncmp("200 ", decReponse, 4) == 0) printf("%s\n", decReponse + 4);
			else printf("%s%s%s%s\n", DPRE, RED, decReponse, RESET);

			codeRetour = (int)strtol(decReponse, NULL, 10);
			free(decReponse);
		}
		free(reponse);
		return codeRetour;
	}
}
