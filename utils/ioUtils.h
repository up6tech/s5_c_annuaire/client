//
// Created by fabien on 23/01/2021.
//


#ifndef IO_UTILS
#define IO_UTILS
#include <stddef.h>

#define RED   "\x1B[31m"
#define GREEN   "\x1B[32m"
#define YELLOW   "\x1B[33m"
#define BLUE   "\x1B[34m"
#define MAGENTA   "\x1B[35m"
#define CYAN   "\x1B[36m"
#define WHITE   "\x1B[37m"
#define RESET "\x1B[0m"

#define IPRE GREEN " > " RESET
#define DPRE RED " ! " RESET
#define WPRE YELLOW " ? " RESET
#define IMEN CYAN " -> " RESET
#define IMENS BLUE " -> " RESET

/**
 * Code pris sur https://stackoverflow.com/questions/34134074/c-size-of-two-dimensional-array
 * réponse numéro 3
 */
#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))


extern char *io_userName ;
extern char *io_password;


/**************************************************
 * Encodage et décodage des URI
 **************************************************/

/**
 * Table statique pour l'utilisation de encodeURI()
 */
static const wchar_t rfc3986[] = {
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '-', '.', '\0',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
        'X', 'Y', 'Z', '\0', '\0', '\0', '\0', '_', '\0', 'a', 'b', 'c', 'd',
        'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\0', '\0', '\0', '~', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
        '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' };

/**
 * Demande une saisie à l'utilisateur
 * La chaine retournée est allouée dynamiquement. Donc a free.
 * @author Dorian Gardes
 * @date 30/01/2021
 * @param message Message à afficher pour demander la saisie.
 * @return La chaine saisie par l'utilisateur
 */
char* demanderSaisie(char* message);

/**
 * Demande une saisie d'un nombre à l'utilisateur
 * @author Dorian Gardes
 * @date 30/01/2021
 * @param message Message à afficher pour demander la saisie.
 * @param borneMin Valeur minimale (incluse) attendue
 * @param borneMax Valeur maximale (incluse) attendue
 * @return Le nombre saisi
 */
int demanderSaisieNombre(char* message, int borneMin, int borneMax);

/**
 * Vide l'écran
 * Ne fonctionne pas dans la console intégrée de CLion mais nickel sur un véritable terminal
 */
void clearScreen();

void stripString(char* str);

/**
 * Encode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_encoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string décodée
 * @param to string encodée
 */
char* encodeURI(const char *from);

/**
 * Décode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_decoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string encodée
 * @param to string decodée
 */
char*  decodeURI(const char *from);

/********************************
 *
 * Utilité
 *
 ********************************/

/**
 *
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param req Chaine contenant la requête
 * @param tableau Pointeur vers le tableau défini et rempli par la focntion
 * @return Le nombre d'éléments dans tableau
 */
int decoupageRequete(const char* req, char*** tableau);

/**
 * Met à jour le login et le mot de passe de l'utilisateur dans la mémoire
 * @author Fabien
 *
 * @param char* userName Le nom de l'utilisateur
 * @param char* password
 *
 * @return -1 si erreur, sinon 0
 *
 */
void setLogin(char *userName, char *password);

/**
 * Affichage de la réponse du serveur
 * @author Dorian Gardes
 * @return Code de retour envoyé par le serveur
 */
int printReponse();

#endif
