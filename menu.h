/*
 * menu.h
 *
 *  Created on: 4 janv. 2021
 *      Author: Fabien
 */

#ifndef MENU_H_
#define MENU_H_

#include <stdbool.h>



/**
 * Ouvre le menu utilisateur
 * @author Fabien
 *
 * @param int index - L'index de l'option qu'a selectionné l'utilisateur
 * @param int admin - 1 si l'utilisateur est connecté en tant qu'administrateur, 0 sinon
 */
void ouvrirMenu(int admin);

/**
 * Ouvre le menu de création d'un utilisateur (Admin)
 * @author Fabien
 *
 * @param char** userName - Le pointeur ou doit être stocké le nom utilisateur (Doit être libéré aprés utilisation)
 * @param char** password - Le pointeur ou doit être stocké le mot de passe (Doit être libéré aprés utilisation)
 */
void ouvrirMenuCreerUtilisateur(char **userName, char **password);

/**
 * Ouvre le menu de modification de mot de passe (Admin)
 * @author Fabien
 *
 * @return char* Le pointeur ou doit est stocké le mot de passe (Doit être libéré aprés utilisation)
 */
char* ouvrirMenuChangerMotDePasse();

/**
 * Ouvre le menu de modification d'un utilisateur (changer le mot de passe / le nom) (Admin)
 * @author Fabien
 *
 * @param char* Le pointeur ou est stocké le nom d'utilisateur à modifier, le champ à modifier et la valeur (Doit être libéré aprés utilisation)
 */
char* ouvrirMenuEditerUtilisateur();

char* ouvrirMenuPartage();

char* ouvrirMenuCreerContact();

char* ouvrirMenuSupprimer(bool isPartage);

char* ouvrirMenuEditer();

char* ouvrirMenuLister();

#endif /* MENU_H_ */
