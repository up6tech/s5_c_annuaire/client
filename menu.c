#include "menu.h"
#include "utils/ioUtils.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <malloc.h>


const char *menuUser[] = {
        "Créer un contact", "Partager l'annuaire",
        "Supprimer un contact", "Supprimer un partage",
        "Modifier un contact", "Afficher les contacts", "Modifier son mot de passe"};
const char *menuAdmin[] = {"Afficher les utilisateurs", "Créer un utilisateur", "Supprimer un utilisateur",
                           "Modifier le mot de passe", "Modifier un utilisateur"};

/*
 * menu.c
 *
 *  Created on: 4 janv. 2021
 *      Author: Fabien
 */

void ouvrirMenu(int admin) {

	// On défini le menu en fonction du compte connecté
    const char **userMenu = admin == 1 ? menuAdmin : menuUser;
    int menuSize = admin == 1 ? LEN(menuAdmin) : LEN(menuUser);

    if (admin == 1) {
        printf("%s %s Vous êtes connecté en tant qu'Administrateur%s\n\n", DPRE, RED, RESET);
    }

    int i;
    for (i = 0; i < menuSize; i++) {
        printf("%s (%d) %s\n", IMEN, i + 1, userMenu[i]);
    }
    printf("%s (0) Sortir de l'application\n", IMEN);
    printf("\n");
    printf("%s Entrez le numéro du choix\n", IPRE);
}

void ouvrirMenuCreerUtilisateur(char **userName, char **password) {

    *userName = demanderSaisie("Veuillez saisir le nom du nouvel utilisateur.");
    *password = demanderSaisie("Veuillez saisir le mot de passe du nouvel utilisateur.");
}

char* ouvrirMenuChangerMotDePasse() {

	char* newPassword = demanderSaisie("Veuillez saisir votre nouveau mot de passe.");
    printf("%sVous avez choisi %s en tant que nouveau mot de passe\n", IPRE, newPassword);
	return newPassword;
}

char* ouvrirMenuEditerUtilisateur() {

    char* id = demanderSaisie("Veuillez saisir le numéro de l'utilisateur à modifier.");

    char msg[183];
	sprintf(msg, "Veuillez saisir le champ suivi de la modification à faire (Ex:)\n");
	sprintf(msg + strlen(msg), "%s 'nom gerard'\n", IMEN);
	sprintf(msg + strlen(msg), "%s 'motdepasse caramel123'\n", IMEN);
	sprintf(msg + strlen(msg), "%s Syntaxe : <champ> <valeur>", IPRE);

	char* value = demanderSaisie(msg);

	char* edit = calloc(strlen(id) + strlen(value) +2, sizeof(char));
	strcpy(edit, id);
    strcat(edit, " ");
    strcat(edit, value);

	return edit;
}


char* ouvrirMenuCreerContact() {

    char* req = calloc(9, sizeof(char));
    strcpy(req, "contact");

    char* saisie = NULL;
    char* encSaisie = NULL;
	char msg[82]; //82 Correspond à la taille du plus long message.

	char* preformatedMsg[5] = {
			"Veuillez rentrer le nom du contact %s(Obligatoire)",
			"Veuillez rentrer le prénom du contact %s(Obligatoire)",
			"Veuillez rentrer l'email du contact %s(Obligatoire)",
			"%sVeuillez rentrer son numéro de téléphone %s(Facultatif)",
			"%sVeuillez rentrer son adresse %s(Facultatif)"
	};

    printf("%sVous allez créer un nouveau contact\n", IPRE);

	for (int i = 0; i < 5; ++i) {

		if (i < 3) {
			sprintf(msg, preformatedMsg[i], DPRE);

			while (1) { // On a déjà dit que ces valeurs sont OBLIGATOIRES
				saisie = demanderSaisie(msg);
				if (strcmp(saisie, "") == 0) {
					free(saisie);
				} else break;
			}
		} else {
			sprintf(msg, preformatedMsg[i], WPRE, DPRE, IPRE);
			saisie = demanderSaisie(msg);
		}
		encSaisie = encodeURI(saisie);
		free(saisie);
		req = realloc(req, (strlen(req) + strlen(encSaisie) + 2) * sizeof(char));
		/*if (strlen(encSaisie) > 0) */sprintf(req + strlen(req), " %s", encSaisie);
		free(encSaisie);
	}

    strcat(req, "\n");
	return req;

}

char* ouvrirMenuSupprimer(bool isPartage){

    char* type = (isPartage) ? "du partage" : "du contact";

    char msg[145];
    sprintf(msg, "Veuillez rentrer le numéro %s que vous voulez supprimer\n%sAttention%sCette opération est irrévocable", type, DPRE,DPRE);
    int id = demanderSaisieNombre(msg, 1, INT_MAX);

    type = isPartage ? "partage " : "contact ";
    char* retour = calloc(strlen(type) + (int)log10(id) +2, sizeof(char));
    sprintf(retour, "%s%d", type, id);

	return retour;
}

char* ouvrirMenuEditer() {

    char* id = demanderSaisie("Veuillez saisir le numéro du contact à modifier.");

    printf("%sVeuillez saisir le champ suivi de la modification à faire (Ex:)\n", IPRE);
    printf("%s 'nom caramel'\n", IMEN);
    printf("%s 'prenom gerard'\n", IMEN);
    printf("%sListe des champs : 'nom', 'prenom', 'telephone', 'mail', 'adresse'\n", IPRE);
    printf("%s Syntaxe : <champ> <valeur>\n", IPRE);

	char msg[251];
	sprintf(msg, "Veuillez saisir le champ suivi de la modification à faire (Ex:)\n");
	sprintf(msg + strlen(msg), "%s 'nom caramel'\n", IMEN);
	sprintf(msg + strlen(msg), "%s 'prenom gerard'\n", IMEN);
	sprintf(msg + strlen(msg), "%sListe des champs possibles : 'nom', 'prenom', 'telephone', 'mail', 'adresse'\n", IPRE);
	sprintf(msg + strlen(msg), "%s Syntaxe : <champ> <valeur>", IPRE);

	char* value = demanderSaisie(msg);

	char* edit = calloc(strlen(id) + strlen(value) +2, sizeof(char));
	strcpy(edit, id);
	strcat(edit, " ");
	strcat(edit, value);

	return edit;
}

char* ouvrirMenuLister(){

    char msg[376];
	sprintf(msg, "Vous pouvez afficher vos contacts, entrez 'annuaire'.\n%sPour afficher un compte partagé entrez 'annuaire' suivi du numéro de partage affiché ci-dessus\n%sPour afficher vos partages entrez 'partages'.\n%sEnfin pour afficher les comptes présents sur le serveur entrez 'comptes'.\n", IPRE, IPRE, IPRE);
	sprintf(msg + strlen(msg), "%sChoix possibles : 'annuaire',  'partages' ou 'comptes'", IPRE);
	return demanderSaisie(msg);
}

char* ouvrirMenuPartage() {

	char* id = demanderSaisie("Veuillez saisir l'id du compte avec lequel vous voulez partager votre annuaire.");

	char* sortie = calloc(strlen(id) + 10, sizeof(char));
	sprintf(sortie, "partage %s", id);
    return sortie;
}
