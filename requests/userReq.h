//
// Created by fabien on 23/01/2021.
//

#ifndef CLIENT_USERREQ_H
#define CLIENT_USERREQ_H

void sendCreateShare(char *reqShare);

void sendRemove(char *reqDelete);

void sendDisplayContact(char* choixListe);

void sendEditContacts(char* edit);

#endif //CLIENT_USERREQ_H
