#include "adminReq.h"
#include "../utils/ioUtils.h"
#include "../utils/socketClient.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>

//
// Created by fabien on 23/01/2021.
//

void sendAdminCreateUser(char* userName, char* password){

	char* encodedUserName = encodeURI(userName);
	char* encodedPassword = encodeURI(password);

	char* requete = calloc(strlen(io_userName)
						 + strlen(io_password)
						 + strlen(encodedUserName)
						 + strlen(encodedPassword)
						 + 19, sizeof(char));
    sprintf(requete, "AUPSCG creer %s %s %s %s\n", io_userName, io_password, encodedUserName, encodedPassword);
	free(encodedUserName);
	free(encodedPassword);
    Emission(requete);
    free(requete);
}

void sendAdminRemoveUser(int idUtilisateur){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + (int)log10(idUtilisateur) + 21, sizeof(char));
	sprintf(requete, "AUPSCG detruire %s %s %d\n",  io_userName, io_password, idUtilisateur);
	Emission(requete);
	free(requete);
}

void sendAdminEditUser(char* edit){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(edit) + 21, sizeof(char));
	sprintf(requete, "AUPSCG modifier %s %s %s\n",  io_userName, io_password, edit);
	Emission(requete);
	free(requete);
}

void sendAdminDisplayUsers(){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + 20, sizeof(char));
	sprintf(requete, "AUPSCG afficher %s %s\n",  io_userName, io_password);
	Emission(requete);
	free(requete);
}


void sendAdminEditPassword(char* newPassword){

	char* encodedPassword = encodeURI(newPassword);
	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(encodedPassword) + 23, sizeof(char));
	sprintf(requete, "AUPSCG motdepasse %s %s %s\n",  io_userName, io_password, encodedPassword);
	free(encodedPassword);
	Emission(requete);
	free(requete);
}
