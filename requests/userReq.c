//
// Created by fabien on 23/01/2021.
//
#include "userReq.h"
#include "../utils/ioUtils.h"
#include "../utils/socketClient.h"
#include <stdio.h>
#include <malloc.h>
#include <string.h>

void sendCreateShare(char* req){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(req) + 18, sizeof(char));
    sprintf(requete, "AUPSCG creer %s %s %s\n",  io_userName, io_password, req);
    Emission(requete);
    free(requete);
}

void sendRemove(char *reqDelete){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(reqDelete) + 21, sizeof(char));
    sprintf(requete, "AUPSCG detruire %s %s %s\n", io_userName, io_password,reqDelete);
	Emission(requete);
	free(requete);
}

void sendDisplayContact(char* choixListe){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(choixListe) + 21, sizeof(char));
    sprintf(requete, "AUPSCG afficher %s %s %s\n", io_userName, io_password, choixListe);
	Emission(requete);
	free(requete);
}

void sendEditContacts(char* edit){

	char* requete = calloc(strlen(io_userName) + strlen(io_password) + strlen(edit) + 21, sizeof(char));
	sprintf(requete, "AUPSCG modifier %s %s %s\n", io_userName, io_password, edit);
	Emission(requete);
	free(requete);
}