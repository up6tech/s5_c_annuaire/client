//
// Created by fabien on 23/01/2021.
//

#ifndef CLIENT_ADMINREQ_H
#define CLIENT_ADMINREQ_H

void sendAdminCreateUser(char* userName, char* password);

void sendAdminRemoveUser(int idUser);

void sendAdminEditUser(char* edit);

void sendAdminDisplayUsers();

void sendAdminEditPassword(char* newPassword);

#endif //CLIENT_ADMINREQ_H
