
#include "utils/socketClient.h"
#include "utils/ioUtils.h"
#include "requests/adminReq.h"
#include "requests/userReq.h"
#include "menu.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <signal.h>


bool fin;

void changerMotDePasse() {
    char *newPassword = ouvrirMenuChangerMotDePasse();
    if (Initialisation("localhost") != 1) {
        printf("%sServeur introuvable\n", DPRE);
    } else {
        sendAdminEditPassword(newPassword);
		if (printReponse() == 200) setLogin(NULL, newPassword);
        Terminaison();
    }
    free(newPassword);
}

/**
 * Fonction pour intercepter les signaux et fermer le serveur proprement.
 * @author Dorian Gardes
 * @date 08/01/2021
 * @param signal Numéro de signal
 */
void signalCatcher(int signal) {

	free(io_userName);
	free(io_password);
	clearScreen();
	printf("%sFermeture réussie%s\n", GREEN, RESET);
	exit(EXIT_SUCCESS);
}

int main() {

    // On importe l'encodage courant pour pouvoir gérer les accents correctement
    setlocale(LC_ALL, "");
    clearScreen();

	// On intercepte le Ctrl+C (SIGINT)
	struct sigaction act;
	act.sa_handler = signalCatcher;
	sigaction(SIGINT, &act, NULL);

	bool isAdmin = false;
	bool logged = false; // On vérifie que le mot de passe est ok avant d'afficher le menu

	do {
		// On demande le login de l'utilisateur
		char* userName = demanderSaisie("Veuillez saisir votre nom d'utilisateur.");

		// On demande le mot de passe de l'utilisateur
		char* password = demanderSaisie("Veuillez saisir votre mot de passe.");

		isAdmin = strcmp(userName, "admin") == 0;

		setLogin(userName, password);
		free(userName);

		clearScreen();
		if (Initialisation("localhost") != 1) {
			printf("%sServeur introuvable\n", DPRE);
			free(password);
			return EXIT_SUCCESS;
		} else {
			sendAdminEditPassword(password); // On demande au serveur de modifier le mot de passe par celui déjà défini
											//  Ainsi s'il est faux la requête échoue sinon on le remplace par lui même.
			free(password);
			char* reponse = Reception();
			char* decReponse = decodeURI(reponse);
			free(reponse);
			if (strncmp("AUPSCG 200 ", decReponse, 11) == 0) logged = true;
			else printf("%s%sErreur d'authentification: %s%s", DPRE, RED, decReponse + 7, RESET);

			free(decReponse);
		}

	} while (!logged);


    printf("%s%sConnection réussie !%s\n", IPRE, GREEN, RESET);

    fin = false;

    while (!fin) {

        ouvrirMenu(isAdmin);
        int choice;
        // L'utilisateur est connecté avec le compte admin
        if (isAdmin) {
        	choice = demanderSaisieNombre("Choix : ", 0, 5);
			clearScreen();

			switch (choice) {
				case 0:
					fin = true;
					break;
				case 1:
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendAdminDisplayUsers();
						printReponse();
						Terminaison();
					}
					break;
				case 2: {
					char *createdUserName = NULL;
					char *createdPassword = NULL;
					ouvrirMenuCreerUtilisateur(&createdUserName, &createdPassword);
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendAdminCreateUser(createdUserName, createdPassword);
						printReponse();
						Terminaison();
					}
					free(createdUserName);
					free(createdPassword);
					break;
				}
				case 3: {

					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendAdminDisplayUsers();
						int reponse = printReponse();
						Terminaison();
						if (reponse != 200) break;
						int idUtilisateur = demanderSaisieNombre("Veuillez saisir l'identifiant de l'utilisateur à supprimer (entier)'.", 0, INT_MAX);
						if (Initialisation("localhost") != 1) {
							printf("%sServeur introuvable\n", DPRE);
						} else {
							sendAdminRemoveUser(idUtilisateur);
							printReponse();
							Terminaison();
						}
					}
					break;
				}
				case 4:
					changerMotDePasse();
					break;
				case 5:
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						// D'abord on affiche les utilisateurs
						sendAdminDisplayUsers();
						printf("%sListe des utilisateurs\n", IPRE);
						if (printReponse() == 200) {
							Terminaison();
							char* edit = ouvrirMenuEditerUtilisateur();
							if (Initialisation("localhost") != 1) {
								printf("%sServeur introuvable\n", DPRE);
							} else {
								sendAdminEditUser(edit);
								printReponse();
							}
							free(edit);
						}
						Terminaison();
					}
					break;
				default: // Impossible car valeur sécurisée par demanderSaisieNombre()
					break;
			} // Fin du switch administrateur

            // L'utilisateur est connecté avec un compte lambda
        } else {
			choice = demanderSaisieNombre("Choix : ", 0, 7);
			clearScreen();

            switch (choice) {
                case 0:
                	fin = true;
                    break;
                case 1: {
                    char *reqCreate = ouvrirMenuCreerContact();
                    if (Initialisation("localhost") != 1) {
                        printf("%sServeur introuvable\n", DPRE);
                    } else {
                        sendCreateShare(reqCreate);
                        printReponse();
                        Terminaison();
                    }
                    free(reqCreate);
                    break;
                }
                case 2: {

					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendDisplayContact("comptes");
						int reponse = printReponse();
						Terminaison();
						if (reponse != 200) break;
						char *reqShare = ouvrirMenuPartage();
						if (Initialisation("localhost") != 1) {
							printf("%sServeur introuvable\n", DPRE);
						} else {
							sendCreateShare(reqShare);
							printReponse();
							Terminaison();
						}
						free(reqShare);
					}
                    break;
                }
                case 3:{
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendDisplayContact("annuaire");
						printf("%sListe de vos contacts / des utilisateurs\n", IPRE);
						int reponse = printReponse();
						Terminaison();
						if (reponse != 200) break;
						char *reqDelete = ouvrirMenuSupprimer(false);
						if (Initialisation("localhost") != 1) {
							printf("%sServeur introuvable\n", DPRE);
						} else {
							sendRemove(reqDelete);
							printReponse();
							Terminaison();
						}
						free(reqDelete);
					}
                    break;
                }
                case 4:{
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendDisplayContact("partages");
						printf("%sListe de vos partages\n", IPRE);
						int reponse = printReponse();
						Terminaison();
						if (reponse != 200) break;
						char *reqDelete = ouvrirMenuSupprimer(true);
						if (Initialisation("localhost") != 1) {
							printf("%sServeur introuvable\n", DPRE);
						} else {
							sendRemove(reqDelete);
							printReponse();
							Terminaison();
						}
						free(reqDelete);
					}
                    break;
                }
                case 5:{
                    char *reqEdit = ouvrirMenuEditer();
                    if (Initialisation("localhost") != 1) {
                        printf("%sServeur introuvable\n", DPRE);
                    } else {
                        sendEditContacts(reqEdit);
                        printReponse();
                        Terminaison();
                    }
                    free(reqEdit);
                    break;
                }
                case 6: {
					if (Initialisation("localhost") != 1) {
						printf("%sServeur introuvable\n", DPRE);
					} else {
						sendDisplayContact("partages");
						printf("%sListe de vos partages\n", IPRE);
						int reponse = printReponse();
						Terminaison();
						if (reponse != 200) break;
						char *choixMenu = ouvrirMenuLister();
						if (Initialisation("localhost") != 1) {
							printf("%sServeur introuvable\n", DPRE);
						} else {
							sendDisplayContact(choixMenu);
							printf("%sListe de vos contacts / des utilisateurs\n", IPRE);
							printReponse();
							Terminaison();
						}
						free(choixMenu);
					}
                    break;
                }
                case 7:
                    changerMotDePasse();
                    break;
                default:
                    break;
            }
        }
    }

	free(io_userName);
	free(io_password);
	clearScreen();

    return EXIT_SUCCESS;
}
